import React, {useState, useRef} from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  StyleSheet,
  View,
  FlatList,
} from 'react-native';
import {useQuery, useMutation} from '@apollo/client';
import {ADD_TODO, DELETE_TODO, FETCH_TODOS} from './queries';
import Todo from './Todo';

const HomeScreen = () => {
  const refInput = useRef();

  const [content, setContent] = useState('');

  const {data, loading, refetch} = useQuery(FETCH_TODOS);

  const [createTodo] = useMutation(ADD_TODO);

  const [deleteTodo] = useMutation(DELETE_TODO);

  const onCreate = async () => {
    if (!content) {
      return;
    }
    await createTodo({variables: {data: {content}}});
    setContent('');
    await refetch();
    if (refInput && refInput.current) {
      refInput.current.focus();
    }
  };

  const onDelete = async (id) => {
    if (!id) {
      return;
    }
    await deleteTodo({variables: {id}});
    await refetch();
  };

  const renderLoading = () => {
    return loading && <ActivityIndicator style={{marginTop: 30}} />;
  };

  const renderTodo = ({item, index}) => {
    const {content, id} = item;

    return (
      <Todo
        content={content}
        key={`key_${index}`}
        onDelete={() => onDelete(id)}
      />
    );
  };

  return (
    <View style={styles.container}>
      {renderLoading()}
      <View style={styles.header}>
        <TextInput
          ref={refInput}
          value={content}
          onChangeText={(e) => setContent(e)}
          style={styles.textInput}
          selectionColor={'black'}
          onSubmitEditing={onCreate}
        />
        <View style={styles.wrapButton}>
          <TouchableOpacity
            onPress={onCreate}
            style={[styles.button, content && styles.buttonActive]}
            disabled={!content}>
            <Text
              style={[styles.titleButton, content && styles.titleButtonActive]}>
              {'Create'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
        data={data?.todos}
        renderItem={renderTodo}
        contentContainerStyle={styles.content}
        ListEmptyComponent={() => (
          <Text style={styles.emptyText}>{'Empty todo'}</Text>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  content: {
    alignItems: 'center',
    flexDirection: 'column-reverse',
  },
  header: {
    width: '100%',
    marginTop: 60,
  },
  wrapButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    marginVertical: 30,
    paddingVertical: 3,
    paddingHorizontal: 10,
    borderWidth: 2,
    borderColor: '#00B2BF',
    borderRadius: 20,
  },
  buttonActive: {
    backgroundColor: '#00B2BF',
  },
  titleButton: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#00B2BF',
  },
  titleButtonActive: {
    color: 'white',
  },
  textInput: {
    height: 40,
    fontSize: 40,
    color: 'black',
    paddingVertical: 3,
    borderColor: '#00B2BF',
    borderBottomWidth: 1,
    marginHorizontal: 70,
  },
  emptyText: {
    fontSize: 20,
    color: '#00B2BF',
  },
});

export default HomeScreen;
