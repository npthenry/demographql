import React from 'react';
import {View, TouchableOpacity, StyleSheet, Image, Text} from 'react-native';

const Todo = (props) => {
  const {content, onDelete} = props;
  return (
    <View style={styles.container}>
      <Text style={styles.content}>{content}</Text>
      <TouchableOpacity onPress={onDelete} style={styles.wrapIcon}>
        <Image source={require('./trash.png')} style={styles.trash} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 250,
    height: 40,
    borderColor: '#00B2BF',
    borderWidth: 0.5,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginBottom: 10,
  },
  content: {
    fontSize: 20,
  },
  wrapIcon: {
    position: 'absolute',
    right: 5,
  },
  trash: {
    height: 20,
    width: 20,
  },
});
export default Todo;
