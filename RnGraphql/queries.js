import gql from 'graphql-tag';

// Query to fetch all the to-dos
const FETCH_TODOS = gql`
  query FetchTodo {
    todos {
      id
      content
      done
    }
  }
`;

// Mutation to add a to-do.
const ADD_TODO = gql`
  mutation AddTodo($data: CreateTodoInput!) {
    createTodo(data: $data) {
      id
      content
      done
    }
  }
`;

// Mutation to delete a to-do
const DELETE_TODO = gql`
  mutation DeleteTodo($id: ID!) {
    deleteTodo(id: $id) {
      id
      content
      done
    }
  }
`;

// Export all the queries and mutations
export {FETCH_TODOS, ADD_TODO, DELETE_TODO};
